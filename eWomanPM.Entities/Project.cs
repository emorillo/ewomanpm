﻿using eWomanPM.Core;
using eWomanPM.Utilities.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace eWomanPM.Entities
{
    [Table("Projects")]
    public class Project   : Base
    {
        [LoggingPrimaryKey]
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Technology { get; set; }
        public int ProjectTypeId { get; set; }
        // public virtual ProjectType ProjectType { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public int StatusId { get; set; }
        [IgnoreLoggingAttribute]
        public virtual Status Status { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime EndDate { get; set; }
        public decimal Cost { get; set; }
        public DateTime Date { get; set; }
    }
}