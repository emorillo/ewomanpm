﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace eWomanPM.Entities
{
    [Table("TrackChanges")]
    public class TrackChange
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string PropertyName { get; set; }
        public int PrimaryKey { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime DateChanged { get; set; }
    }
}