﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace eWomanPM.Entities
{
    [Table("Documents")]
    public class Document
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int ProjectId { get; set; }
        public string Path { get; set; }
    }
}