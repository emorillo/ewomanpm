﻿
using eWomanPM.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace eWomanPM.Data.Context
{
    public class eWomanPMContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<TrackChange> TrackChanges { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<ProjectType> ProjectType { get; set; }
    }
}