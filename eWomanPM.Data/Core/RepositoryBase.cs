﻿using eWomanPM.Core;
using eWomanPM.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace eWomanPM.Data.Core
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        private eWomanPMContext _context;
        public DbContext Context { get; set; }
        protected DbSet<T> Set { get; set; }

        string errorMessage = string.Empty;

        public eWomanPMContext Context1
        {
            get { return _context; }
            set
            {
                _context = value;
                Set = value.Set<T>();
            }
        }

        public RepositoryBase()
            : this(new eWomanPMContext())
        {
        }

        public RepositoryBase(eWomanPMContext context)
        {
            Context = context;
            Set = Context.Set<T>();
        }


        public virtual IQueryable<T> GetAll()
        {
            return Set.AsQueryable();
        }

        public virtual T Add(T entity)
        {
            Set.Add(entity);
            return entity;
            //T result;
            //try
            //{
            //    if (entity == null)
            //    {
            //        throw new ArgumentNullException("entity");
            //        result = Set.Add(entity);
            //    }
            //catch (DbEntityValidationException dbEx)
            //{

            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            errorMessage += string.Format("Property: {0} Error: {1}",
            //            validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
            //        }
            //    }
            //    throw new Exception(errorMessage, dbEx);
            //}

            //return result;
        }

        public virtual T Update(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);
             
            if (entry.State == EntityState.Detached)
            {
                Set.Attach(entity);
            }
            else
            {
                Context.Entry(entity).State = EntityState.Modified;
            }

            return entity;
        }

        public virtual T Delete(T entity)
        {
            DbEntityEntry entry = Context.Entry(entity);
         
            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                Set.Attach(entity);
                Set.Remove(entity);
            }


            return entity;
        }

        public virtual int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public virtual void Dispose()
        {
            //Dispose();
            Context.Dispose();
        }

        public T Find(int id)
        {
            return Set.Find(id);
        }
        public T GetById(int id)
        {
            return Set.Find(id);
        }
        public void Delete(int id)
        {
            T entity = Set.Find(id);

            if (entity == null)
            {
                return; // ya fue borrada
            }

            Delete(entity);
        }

        public virtual void Delete(ICollection<T> entities)
        {
            //foreach (T entity in entities)
            //{
            //    _objectSet.DeleteObject(entity);
            //}

            foreach (T entity in entities)
            {
                Delete(entity);
            }
        }
        public virtual void Delete(List<T> entities)
        {

            foreach (T entity in entities)
            {
                Delete(entity);
            }
        }


        public virtual void Delete(Expression<Func<T, bool>> lambda)
        {
            List<T> entities = Find(lambda).ToList();
            Delete(entities);
        }

        public virtual IQueryable<T> Find(Expression<Func<T, bool>> lambda)
        {
            return Set.Where(lambda).AsQueryable();
        }
      
    }
    
}