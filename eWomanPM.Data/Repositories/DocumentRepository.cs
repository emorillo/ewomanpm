﻿using eWomanPM.Data.Context;
using eWomanPM.Data.Core;
using eWomanPM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Data.Repositories
{
    public class DocumentRepository : RepositoryBase<Document>
    {
        public DocumentRepository() : base()
        {

        }

        public DocumentRepository(eWomanPMContext _context) : base(_context)
        {

        }
    }
}
