﻿using eWomanPM.Data.Context;
using eWomanPM.Data.Core;
using eWomanPM.Entities;
using eWomanPM.Entities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Data.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public UserRepository() : base()
        {

        }

        public UserRepository(eWomanPMContext _context) : base(_context)
        {

        }
        List<User> users = new List<User>() {

        new User() {Email="abc@gmail.com",Roles="Admin,Editor",Password="abcadmin" },
        new User() {Email="xyz@gmail.com",Roles="Editor",Password="xyzeditor" }
              };

        public User GetUserDetails(User user)
        {
            return users.Where(u => u.Email.ToLower() == user.Email.ToLower() &&
            u.Password == user.Password).FirstOrDefault();
        }
    }
}
