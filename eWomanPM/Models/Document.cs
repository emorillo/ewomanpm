﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Models
{
    public class Document
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime Date { get; set; }
        public int ProjectId { get; set; }
    }
}