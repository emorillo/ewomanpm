﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Technology { get; set; }
        public int ProjectType { get; set; }
        public DateTime Date { get; set; }
        public int Status { get; set; }
        public DateTime StarDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Cost { get; set; }
    }
}