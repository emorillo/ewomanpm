﻿using eWomanPM.Entities;
using eWomanPM.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace eWomanPM.WebApi.Controllers
{
    public class ProjectController : ApiController
    {
        //GET: api/Project
        ProjectService _projectService = new ProjectService();
        [ResponseType(typeof(Project))]
        public IHttpActionResult Get()
        {
            var projects = _projectService.GetAll().ToList();

            return Ok(projects);
        }

        // GET: api/Project/5
        [ResponseType(typeof(Project))]
        public IHttpActionResult Get(int id)
        {
            var project = _projectService.GetById(id);
            if (project  == null)
            {
                NotFound();
            }
            return Ok(project);
        }

        // POST: api/Project
        public IHttpActionResult Post(Project project)
        {
            _projectService.AddProject(project);
            _projectService.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = project.Id }, project);
        }

        // PUT: api/Project/5
        public IHttpActionResult Put(int id, Project project)
        {
            if (id != project.Id)
            {
                return BadRequest();
            }
            _projectService.Update(project);
            try
            {
                _projectService.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new Exception(ex.Message);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Project/5
        [ResponseType(typeof(Project))]
        public IHttpActionResult Delete(int id)
        {
            var project = _projectService.GetById(id);
            if (project == null)
            {
                NotFound();
            }
            _projectService.Delete(project);
            _projectService.SaveChanges();
            return Ok(project);
        }
    }
}
