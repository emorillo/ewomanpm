﻿using eWomanPM.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eWomanPM.WebApi.Controllers
{
    public class ChangeController : ApiController
    {
        ChangeLogService _logService = new ChangeLogService();
        public IHttpActionResult Get()
        {
            var logs = _logService.GetAll();

            return Ok(logs);
        }
    }
}
