﻿using eWomanPM.Entities;
using eWomanPM.Service;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eWomanPM.WebApi.Controllers
{
    public class DocumentController : ApiController
    {
        DocumentService _documentService = new DocumentService();
        public IHttpActionResult Get()
        {
            var documents = _documentService.GetAll();

            return Ok(documents);
        }
        public IHttpActionResult Post(Document document)
        {
            _documentService.AddDocument(document);
            _documentService.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = document.Id }, document);
        }
        public IHttpActionResult Put(int id, Document document)
        {
            if (id != document.Id)
            {
                return BadRequest();
            }
            _documentService.Update(document);
            try
            {
                _documentService.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {

                throw new Exception(ex.Message);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
