﻿using eWomanPM.MVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace eWomanPM.MVC.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult UploadDocument()
        {
            return View();
        }
        public ActionResult DownloadFile(int documentId)
        {
            string filename = "Pregunta 12345678910.docx";
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "Documents/" + filename;
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file, int projectId)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Documents/"), fileName);
                file.SaveAs(path);
                DocumentModel documentModel = new DocumentModel();
                documentModel.Date = DateTime.Now;
                documentModel.Name = fileName;
                documentModel.ProjectId = projectId;
                documentModel.Path = path;
                HttpResponseMessage response = GlobalVariables.webApiClient.PostAsJsonAsync("api/Document/", documentModel).Result;
            }

            return RedirectToAction("Index", "Project");
        }
    }
}