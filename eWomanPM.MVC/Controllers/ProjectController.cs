﻿using eWomanPM.MVC.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace eWomanPM.MVC.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Project

        [HttpPost]
        public ActionResult Index(string name, string clientName, int type)
        {
            FillDropdown();
            HttpResponseMessage reponse = GlobalVariables.webApiClient.GetAsync("api/Project").Result;
            var resut = reponse.Content.ReadAsAsync<IEnumerable<ProjectModel>>().Result;
            var list = resut.AsQueryable();
            if (!string.IsNullOrEmpty(name))
            {
                list = list.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(clientName))
            {
                list = list.Where(p => p.ClientName.Contains(clientName));
            }
            if (type > 0)
            {
                list = list.Where(p => p.ProjectTypeId == type);
            }
            return View(list);
        }
        public ActionResult Index()
        {
            FillDropdown();
            HttpResponseMessage reponse = GlobalVariables.webApiClient.GetAsync("api/Project").Result;
            var resut = reponse.Content.ReadAsAsync<IEnumerable<ProjectModel>>().Result;
            return View(resut);
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Project/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.webApiClient.DeleteAsync("api/Project/" + id.ToString()).Result;
            TempData["SucessMessage"] = "Deleted succesfully";
            return RedirectToAction("Index");
        }
        private void FillDropdown()
        {
            List<SelectListItem> projectTypes = new List<SelectListItem>
                {
                  new SelectListItem {  Text = "Select One", Value = "0"},
                  new SelectListItem {  Text = "Fixed Price ", Value = "1"},
                     new SelectListItem { Text = "Time & Material", Value = "2"},

                     };
            List<SelectListItem> tecnologies = new List<SelectListItem>
                {
                  new SelectListItem {  Text = "Select One ", Value = "0"},
                  new SelectListItem {  Text = "C# ", Value = "1"},
                     new SelectListItem { Text = "PHP", Value = "2"},
                       new SelectListItem { Text = "Java", Value = "3"},
                     };
            ViewBag.ProjectTypes = projectTypes;
            ViewBag.Tecnologies = tecnologies;
        }
        public ActionResult AddOrEdit(int id = 0, bool disabled = false)
        {
            FillDropdown();
            ViewBag.disabled = disabled;
            if (id == 0)
            {
                return View(new ProjectModel());
            }
            else
            {

                HttpResponseMessage reponse = GlobalVariables.webApiClient.GetAsync("api/Project/" + id.ToString()).Result;

                HttpResponseMessage reponseDocument = GlobalVariables.webApiClient.GetAsync("api/Document").Result;
                var resut = reponseDocument.Content.ReadAsAsync<IEnumerable<ProjectModel>>().Result;
                ViewBag.Files = resut;

                HttpResponseMessage reponsetrackerChange = GlobalVariables.webApiClient.GetAsync("api/Change").Result;
                var trackerChangeResult = reponsetrackerChange.Content.ReadAsAsync<IEnumerable<TrackerChangeModel>>().Result;
                ViewBag.Files = resut;
                ViewBag.TrackerChanges = trackerChangeResult;
                return View(reponse.Content.ReadAsAsync<ProjectModel>().Result);
            }
            return View(new ProjectModel());
        }
        [HttpPost]
        public ActionResult AddOrEdit(ProjectModel projectModel)
        {
            FillDropdown();
            if (projectModel.Id == 0)
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PostAsJsonAsync("api/Project/", projectModel).Result;
                TempData["SucessMessage"] = "Added succesfully";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PutAsJsonAsync("api/Project/" + projectModel.Id.ToString(), projectModel).Result;
                TempData["SucessMessage"] = "Modified succesfully";
            }

            return RedirectToAction("Index");
        }
        // POST: Project/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
