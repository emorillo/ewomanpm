﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eWomanPM.MVC.Models
{
    public class CustomValidation
    {
        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
        public sealed class IsValidType : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if (value != null)
                {
                    var type = Convert.ToInt16(value);
                    if (type == 0)
                    {
                        return new ValidationResult("Type is required");
                    }
                }
                return ValidationResult.Success;
            }
        }
    }
}
