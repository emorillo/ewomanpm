﻿
using eWomanPM.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eWomanPM.MVC.Models
{
    public class ProjectModel
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "This field is required")]

        [DisplayName("Client Name")]
        public string ClientName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Description { get; set; }
        public int Technology { get; set; }
        //public string TechnologyCast { get; set; }

        [DisplayName("Type")]
        [Models.CustomValidation.IsValidType(ErrorMessage = "Type is required")]
        public int ProjectTypeId { get; set; }
        public virtual ProjectType ProjectType { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime Date { get; set; } = DateTime.Now.Date;
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime StartDate { get; set; } = DateTime.Now.Date;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime EndDate { get; set; } = DateTime.Now.Date;
        [Models.CustomValidation.IsValidType(ErrorMessage = "Cost is required")]
        public decimal Cost { get; set; }
    }
}