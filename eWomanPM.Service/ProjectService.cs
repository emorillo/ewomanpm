﻿using eWomanPM.Data.Context;
using eWomanPM.Data.Repositories;
using eWomanPM.Entities;
using eWomanPM.Utilities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Service
{
    public class ProjectService
    {
        ProjectRepository _proyectRepository;
        eWomanPMContext dbContext = new eWomanPMContext();
        TrackChangeRepository _trackRepository;
        public ProjectService()
        {
            _proyectRepository = new ProjectRepository(dbContext);
            _trackRepository = new TrackChangeRepository(dbContext);
        }
        public void AddProject(Project project)
        {
            try
            {
                project.StatusId = 1;
                _proyectRepository.Add(project);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public void Update(Project project)
        {
            List<ChangeLog> changes = new List<ChangeLog>();
            var result = true;
            try
            {
                var oldProject = _proyectRepository.Find(project.Id);
                ChangeLogService changeService = new ChangeLogService();
                changes = changeService.GetChanges(oldProject, project);
                oldProject.Name = project.Name;
                oldProject.Technology = project.Technology;
                oldProject.ClientName = project.ClientName;
                oldProject.ProjectTypeId = project.ProjectTypeId;
                oldProject.StartDate = project.StartDate;
                oldProject.EndDate = project.EndDate;
                oldProject.Cost = project.Cost;
                oldProject.Date = project.Date;
                _proyectRepository.Update(oldProject);

            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception(ex.Message);
            }
            finally
            {
                if (result)
                {
                    if (changes.Any())
                    {

                        foreach (var item in changes)
                        {
                            TrackChange track = new TrackChange
                            {
                                ClassName = item.ClassName,
                                DateChanged = item.DateChanged,
                                NewValue = item.NewValue,
                                OldValue = item.OldValue,
                                PrimaryKey = item.PrimaryKey,
                                PropertyName = item.PropertyName,

                            };
                            _trackRepository.Add(track);
                        }
                    }

                }
            }

        }
        public Project GetById(int id)
        {
            try
            {

                return _proyectRepository.GetById(id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public Project Delete(Project project)
        {
            try
            {
                project.StatusId = 4;
                Update(project);

                return project;//_proyectRepository.Delete(project);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IQueryable<Project> GetAll()
        {
            IQueryable<Project> result = null;
            try
            {
                result = _proyectRepository.GetAll();
            }
            catch (Exception ex)
            {

                throw;
            }
            return result;
        }
        public void SaveChanges()
        {
            try
            {
                _proyectRepository.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}