﻿using eWomanPM.Data.Context;
using eWomanPM.Data.Repositories;
using eWomanPM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eWomanPM.Service
{
    public class DocumentService
    {
        DocumentRepository _documentRepository;
        eWomanPMContext dbContext = new eWomanPMContext();

        public DocumentService()
        {
            _documentRepository = new DocumentRepository(dbContext);

        }
        public void AddDocument(Document document)
        {
            try
            {
                _documentRepository.Add(document);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public void Update(Document document)
        {
            var result = true;
            try
            {
                var old = _documentRepository.Find(document.Id);
                _documentRepository.Update(old);

            }
            catch (Exception ex)
            {
                result = false;
                throw new Exception(ex.Message);
            }
        }
        public IQueryable<Document> GetAll()
        {
            try
            {
                return _documentRepository.GetAll();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public void SaveChanges()
        {
            try
            {
                _documentRepository.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}