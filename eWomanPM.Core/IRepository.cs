﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eWomanPM.Core
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T GetById(int id);
        T Add(T entity);
        T Delete(T entity);
        T Update(T entity);
        IQueryable<T> GetAll();
        int SaveChanges();
    }
}
